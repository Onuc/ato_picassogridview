package com.example.technical.picassogridview;

import java.util.ArrayList;

class MoviesHost {
    static volatile ArrayList<Movie> movies = new ArrayList<Movie>();
    static volatile boolean moviesSet = false;
}
