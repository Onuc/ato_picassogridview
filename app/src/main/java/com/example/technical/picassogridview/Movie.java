package com.example.technical.picassogridview;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Movie implements Serializable{
    private long tmdbId;
    private String title;
    private String imageUrl;
    private String overview;
    private Date releaseDate;
    private float averageRating;
    public static final SimpleDateFormat DEFAULT_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");



    public Movie(long tmdbId, String title) {
        this(tmdbId,title,"","",new Date(),0f);
    }

    public Movie(long tmdbId, String title, String imageUrl, String overview, Date releaseDate, float averageRating) {
        this.tmdbId = tmdbId;
        this.title = title;
        this.imageUrl = imageUrl;
        this.overview = overview;
        this.releaseDate = releaseDate;
        this.averageRating = averageRating;
    }


    public Movie setTmdbId(long tmdbId) {
        this.tmdbId = tmdbId;
        return this;
    }

    public Movie setTitle(String title) {
        this.title = title;
        return this;
    }

    public Movie setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
        return this;
    }

    public Movie setOverview(String overview) {
        this.overview = overview;
        return this;
    }

    public Movie setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
        return this;
    }

    public Movie setAverageRating(float averageRating) {
        this.averageRating = averageRating;
        return this;
    }


    public long getTmdbId() {
        return this.tmdbId;
    }

    public String getTitle() {
        return this.title;
    }

    public String getImageUrl() {
        return this.imageUrl;
    }

    public String getOverview() {
        return this.overview;
    }

    public Date getReleaseDate() {
        return this.releaseDate;
    }

    public String getReleaseDateAsString() {
        return Movie.DEFAULT_DATE_FORMAT.format(this.releaseDate);
    }

    public float getAverageRating() {
        return this.averageRating;
    }

    /**
     * Parses a date String that is in DEFAULT_DATE_FORMAT to a Date object.
     * @param date string in the DEFAULT_DATE_FORMAT
     * @return Date object holding the date in the date String.
     * @throws java.text.ParseException
     */
    public static final Date getDateFromString(String date) throws ParseException {
        return Movie.DEFAULT_DATE_FORMAT.parse(date);
    }

}
