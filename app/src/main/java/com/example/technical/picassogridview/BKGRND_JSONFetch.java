package com.example.technical.picassogridview;

import android.app.Activity;
import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

class BKGRND_JSONFetch extends AsyncTask<String,Movie,ArrayList<Movie>>{
    private MainActivity targetActivity = MainActivity.currentMainActivity;


    @Override
    protected ArrayList<Movie> doInBackground(String... params) {
        ArrayList<Movie> movies = new ArrayList<Movie>();
        try {
            JSONObject jsonObject = this.getJSONObject(params[0]);
            JSONArray results = jsonObject.getJSONArray("results");
            for (int i = 0; i < results.length(); i++) {
                movies.add(this.getMovie(results.getJSONObject(i)));
            }
        } catch (JSONException | IOException e) {
            Toaster.showToast(this.targetActivity,e.getMessage(),false);
        }
        return movies;
    }

    @Override
    protected void onPostExecute(ArrayList<Movie> movies) {
        MoviesHost.movies = movies;
        MoviesHost.moviesSet = true;
        this.targetActivity.populateMovieView();
    }




    private Movie getMovie(JSONObject jsonObject) throws JSONException {
        long tmdbId = Long.parseLong(jsonObject.getString("id"));
        String title = jsonObject.getString("original_title");
        String imageUrl = "http://image.tmdb.org/t/p/w185/"+jsonObject.getString("poster_path");
        String overview = jsonObject.getString("overview");

        Date releaseDate;
        {
            String[] dateData = jsonObject.getString("release_date").split("-");
            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.YEAR,Integer.parseInt(dateData[0]));
            calendar.set(Calendar.MONTH,Integer.parseInt(dateData[1]));
            calendar.set(Calendar.DAY_OF_MONTH,Integer.parseInt(dateData[2]));
            releaseDate = calendar.getTime();
        }
        float voteAverage = Float.parseFloat(jsonObject.getString("vote_average"));
        return new Movie(tmdbId,title,imageUrl,overview,releaseDate,voteAverage);
    }

    private JSONObject getJSONObject(String urlString) throws IOException, JSONException {
        StringBuffer stringBuffer = new StringBuffer();
        URL url = new URL(urlString);
        HttpURLConnection httpURLConnection = null;
        try {
            httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.connect();
            BufferedInputStream bufferedInputStream = new BufferedInputStream(httpURLConnection.getInputStream());
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(bufferedInputStream));

            String line;
            while ((line = bufferedReader.readLine())!=null) {
                stringBuffer.append(line);
            }

            return new JSONObject(stringBuffer.toString());
        } finally {
            httpURLConnection.disconnect();
        }
    }
}
