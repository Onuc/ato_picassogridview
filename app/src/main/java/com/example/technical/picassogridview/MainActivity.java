package com.example.technical.picassogridview;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.GridView;


public class MainActivity extends ActionBarActivity {
    static MainActivity currentMainActivity;
    private GridView gridViewImages;
    private static final String JSON_URL = "http://api.themoviedb.org/3/discover/movie?sort_by=popularity.desc&api_key=18147b0826078c6d5e462bf97f3e032d";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        MainActivity.currentMainActivity = this;
        this.init();
    }


    private void init(){
        this.gridViewImages = (GridView) this.findViewById(R.id.main_grid_view_movies);
        new BKGRND_JSONFetch().execute(JSON_URL);
    }

    void populateMovieView() {
        MoviesAdapter gridViewImageAdapter = new MoviesAdapter(this, MoviesHost.movies.toArray(new Movie[0]));
        this.gridViewImages.setAdapter(gridViewImageAdapter);
    }
}
