package com.example.technical.picassogridview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

class MoviesAdapter extends BaseAdapter{
    private Movie[] movies;
    private Context context;
    private LayoutInflater layoutInflater;

    public MoviesAdapter(Context context,Movie[] movies) {
        this.context = context;
        this.movies = movies;
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return this.movies.length;
    }

    @Override
    public Object getItem(int position) {
        return this.movies[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView==null){
            convertView = this.layoutInflater.inflate(R.layout.cell,null);
        }
        Movie currentMovie = this.movies[position];
        TextView textViewTitle = (TextView) convertView.findViewById(R.id.cell_text_view_title);
        ImageView imageViewPoster = (ImageView) convertView.findViewById(R.id.cell_image_view_poster);
        textViewTitle.setText(currentMovie.getTitle());
        Picasso.with(this.context).load(currentMovie.getImageUrl()).into(imageViewPoster);

        return convertView;
    }
}
